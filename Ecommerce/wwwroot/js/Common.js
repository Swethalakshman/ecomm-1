﻿function textToAudio(msg = "Please, Enter Mobile Number For Login") {

    //let msg = document.getElementById("text-to-speech").value;
    let speech = new SpeechSynthesisUtterance();
    speech.lang = "en-US";

    speech.text = msg;
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;
    //var ut = new SpeechSynthesisUtterance(msg);
    //ut.speak();
    window.speechSynthesis.speak(speech);
 
    return true;
}

function runSpeechRecognition(obj, appedEle =null) {
    //appedEle.val('');//erase the text in input field
    // get output div reference
    var output = document.getElementById("output");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();
    recognition.continuous = true;

    // This runs when the speech recognition service starts
    recognition.onstart = function () {
        if (speechHint != "search") {
            obj.html("<small>listening, please speak...</small>");
        }
    };

    recognition.onspeechend = function () {
        if (speechHint != "search") {
            obj.html("<small>stopped listening...</small>");
            ApplySearch();
        }
        //recognition.stop();
       // repeatSpeechListinig(obj, appedEle);
    }

    // This runs when the speech recognition service returns result
    recognition.onresult = function (event) {
        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        if (speechHint == "search") {
            $("#Search").val(transcript);
            getProduct(transcript);

        } else {
            appedEle.val(transcript);
            validateMobileNumber(appedEle);
        }
       
        setInterval(repeatSpeechListinig(obj, appedEle), 10000); 
    };
    
    recognition.start();
}
function repeatSpeechListinig(obj, appedEle) {
    runSpeechRecognition(obj, appedEle)
   // setInterval(runSpeechRecognition(obj, appedEle), 10000); 
    
}
function validateMobileNumber(ele) {
    if (ele.val().trim().length < 10) {
        if (textToAudio("Invalid Mobile Number, Please try again")) {
            alert("Invalid Mobile Number")
            runSpeechRecognition($("#voice"), $("#MobileNumber"));
            ele.val('');
        }

    } else {
        save();
    }
}
function getProduct(str) {
    var isAvail = false;
    $(".products").map(function () {
        if ($(this).val().toLowerCase().search(str.trim().toLowerCase()) > -1) {
            isAvail = true;
            var src = $(this).attr('imgsrc')
            var price = $(this).attr('price')
            var div ='<div style="display:flex;width:100%;padding: 10px 0px;border: 1px solid;margin: 10px 0px;">'
            div += '<div style="width:30%;text-align: center;">';
            div += '<img class="product-img" style="top: 9%;"src=' + src+'></div>';
            div += '<div class="right-content">';
            div += '<label style="font-size: 24px;">' + $(this).val()+'</label><br />';
            div += '<label style="font-size: 18px;font-weight: 600;">' + price+' Rs</label><br />';
            div += '</div></div>';
            $('#cartlist').append(div);
            return false;
        }
        
    });
    if (isAvail == false) {
        textToAudio("No Product Found, Please try again");
        alert("No Product Found, Please try again");
    }
    ApplySearch();
}

function ApplySearch() {
    speechHint = "search";
    runSpeechRecognition($("#Search"))
}