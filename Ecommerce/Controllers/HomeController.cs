﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Business Bo;
        public HomeController(ILogger<HomeController> logger, Business Bo)
        {
            _logger = logger;
            this.Bo = Bo;
        }

        public IActionResult Index()
        {
            var rec = Bo.GetProducts();
            var catlist = Bo.GetCategories();
            ViewBag.CategoryList = catlist;
            return View(rec);
        }
        [HttpPost]
        public IActionResult SaveUserByMobileNumber(string MobileNumber)
        {
            var success = -1;
            try
            {
                if (string.IsNullOrEmpty(MobileNumber))
                {
                    return Json("Invalid Mobile Number");
                }
                success = Bo.SaveUserByMobileNumber(MobileNumber);
            }
            catch(Exception e)
            {
                return Json(e.Message);
            }
            
            return Json(success);
        }
      
    }
}
