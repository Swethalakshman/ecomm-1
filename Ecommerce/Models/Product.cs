﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Product : AuditableBase
    {
        [Key]
        public int Id { get; set; }
        public int CatagoryId { get; set; }
        public string ProductName { get; set; }
    }
}
