﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Category : AuditableBase
    {
        [Key]
        public int Id { get; set; }
        public string CatagoryName { get; set; }
    }
}
